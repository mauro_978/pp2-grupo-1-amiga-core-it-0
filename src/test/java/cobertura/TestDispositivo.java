package cobertura;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Dispositivo;
import modelo.Ubicacion;
import modelo.UbicacionDesconocida;

public class TestDispositivo {

	Dispositivo d1, d2, d3, d4;
	
	@Before
	public void setUp() {
		d1 = new Dispositivo("D1");
		d2 = new Dispositivo("D1");
		d3 = new Dispositivo("D2");
		d4 = new Dispositivo(null);
	}
	
	@Test
	public void testGetIDDispositivo() {
		assertEquals(d1.getIdDispositivo(), "D1");
		assertEquals(d2.getIdDispositivo(), "D1");
		assertEquals(d3.getIdDispositivo(), "D2");
		assertEquals(d4.getIdDispositivo(), null);
		
		assertNotEquals(d1.getIdDispositivo(), "D2");
		assertNotEquals(d2.getIdDispositivo(), "D3");
		assertNotEquals(d3.getIdDispositivo(), "D1");
		assertNotEquals(d4.getIdDispositivo(), "D1");
	}
	
	@Test
	public void testGetUbicacionDispositivo() {
		assertTrue(d1.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		assertTrue(d2.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		assertTrue(d3.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		assertTrue(d4.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		
		d1.actualizarUbicacion(new Ubicacion(0,0));
		
		assertTrue(d1.getUbicacionDispositivo().getLatitud()==0);
		assertTrue(d1.getUbicacionDispositivo().getLongitud()==0);
		
		d1.actualizarUbicacion(new Ubicacion(2,1));
		
		assertTrue(d1.getUbicacionDispositivo().getLatitud()==2);
		assertTrue(d1.getUbicacionDispositivo().getLongitud()==1);
		
		assertFalse(d1.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		assertTrue(d1.getUbicacionDispositivo() instanceof Ubicacion);
		assertTrue(d2.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		assertTrue(d3.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		assertTrue(d4.getUbicacionDispositivo() instanceof UbicacionDesconocida);
	}

	
	@Test
	public void testActualizarUbicacionDispositivo() {
		assertTrue(d2.getUbicacionDispositivo() instanceof UbicacionDesconocida);
		
		d2.actualizarUbicacion(new Ubicacion(11,9));
		
		assertTrue(d2.getUbicacionDispositivo().getLatitud()==11);
		assertTrue(d2.getUbicacionDispositivo().getLongitud()==9);
		
		d2.actualizarUbicacion(new Ubicacion(2,1));
		
		assertTrue(d2.getUbicacionDispositivo().getLatitud()==2);
		assertTrue(d2.getUbicacionDispositivo().getLongitud()==1);
	}
	
	@Test
	public void testEqualsDispositivo() {
		assertTrue(d1.equals(d1));
		assertTrue(d1.equals(d2));
		assertFalse(d1.equals(d4));
		assertFalse(d1.equals(d3));
		assertFalse(d1.equals(null));
		
		assertTrue(d2.equals(d1));
		assertTrue(d2.equals(d2));
		assertFalse(d2.equals(d4));
		assertFalse(d2.equals(d3));
		assertFalse(d2.equals(null));
		
		assertFalse(d3.equals(d1));
		assertFalse(d3.equals(d2));
		assertFalse(d3.equals(d4));
		assertTrue(d3.equals(d3));
		assertFalse(d3.equals(null));
		
		assertFalse(d4.equals(d1));
		assertFalse(d4.equals(d2));
		assertTrue(d4.equals(d4));
		assertFalse(d4.equals(d3));
		assertFalse(d4.equals(null));
		
		Integer entero = 7;
		
		assertFalse(d1.equals(entero));
		assertFalse(d2.equals(entero));
		assertFalse(d3.equals(entero));
		assertFalse(d4.equals(entero));
	}
}

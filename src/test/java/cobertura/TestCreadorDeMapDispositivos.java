package cobertura;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import modelo.Dispositivo;
import modelo.Perimetral;
import modelo.Persona;
import setup.CreadorDeMapDispositivos;

public class TestCreadorDeMapDispositivos {

	
	CreadorDeMapDispositivos CMD;
	Set<Perimetral> perimetrales;
	
	@Before
	public void SetUp(){
		CMD = new CreadorDeMapDispositivos();
	}
	
	@Test(expected=NullPointerException.class)
	public void testExcepcionPerimetralesNull() {
		CMD.crearDispositivos(perimetrales);
	}
	
	@Test
	public void testMapVacio() {
		perimetrales = new HashSet<Perimetral>();
		assertTrue((CMD.crearDispositivos(perimetrales)).isEmpty());
	}

	@Test
	public void testMap() {
		Persona agresor = new Persona("40698875","juan","D1");
		Persona agredido = new Persona("42853431","pedro","D2");
		int distanciaPeligro = 10;
		int distanciaPrecaucion = 20;
		Perimetral p = new Perimetral(agresor, agredido, distanciaPeligro, distanciaPrecaucion);
		
		perimetrales = new HashSet<Perimetral>();
		perimetrales.add(p);
		Map<Persona, Dispositivo> mapPD = CMD.crearDispositivos(perimetrales);
		assertTrue(!mapPD.isEmpty());
		assertTrue(mapPD.get(agresor) instanceof Dispositivo);
		assertTrue(mapPD.get(agredido) instanceof Dispositivo);
		assertNotEquals(mapPD.get(agresor), mapPD.get(agredido));
	}

}

package cobertura;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import configuracion.ConfiguradorDeNotificadores;
import interfacesModelo.Notificador;
import interfacesModelo.Observable;

public class TestConfiguradorNotificadores {
	
	ConfiguradorDeNotificadores cn;
	Notificador mecanismoDeNotificacionEmail, mecanismoDeNotificacionTelegram,
				nEMailSpy, nTelegramSpy;
	
	@Before
	public void SetUp() {
		//creo un mecanismo de notificacion por Email
		mecanismoDeNotificacionEmail = new Notificador() {
			
			String nombre = "Email";
			@Override
			public void actualizar(Observable obs) {
//				System.out.println("Notifico por Email");
			}
			@Override
			public String getNombre() {
				return nombre;
			}};
		
		//creo un mecanismo de notificacion por Telegram
		mecanismoDeNotificacionTelegram = new Notificador() {
				
			String nombre = "Telegram";
			@Override
			public void actualizar(Observable obs) {
//				System.out.println("Notifico por Telegram");
			}
			@Override
			public String getNombre() {
				return nombre;
			}};
		
		/*para verificar cuantas veces se llamo al metodo actualizar de un mecanismo 
		 *de notificacion (cuantas veces se indico al notificador que notifique) se 
		 *utilizara un spy el cual nos proporciona Mockito*/	
			
		//creo un spy a partir del mecanismo de notificacion por Email
		nEMailSpy = Mockito.spy(mecanismoDeNotificacionEmail);
		
		//creo un spy a partir del mecanismo de notificacion por Telegram
		nTelegramSpy = Mockito.spy(mecanismoDeNotificacionTelegram);
		
		/*creo una lista de notificadores, agregando los spy en lugar de los
		 *notificadores "reales"*/
		ArrayList<Notificador> notificadores = new ArrayList<Notificador>();
		notificadores.add(nEMailSpy);
		notificadores.add(nTelegramSpy);
		
		//creo el configurador de notificadores con la lista de notificadores con los spy
		cn = new ConfiguradorDeNotificadores(notificadores);
		
	}
	
	/****************************************************************/
	/*   En todos los test se usaran 2 mecanismos de notificacion   */
	/*                    [Email - Telegram]                        */
	/****************************************************************/
	
	@Test
	public void testNotificar1VezANotificadores() {
								
		//le digo al configurador de notificadores que ocurrio un cambio y que debe notificar por los mecanismos
		//de notificacion que se encuentren habilitados
		cn.actualizar(null);
								
		//verifico que notifico 1 vez al mecanismo de notificacion por email 
		verify(nEMailSpy, times(1)).actualizar(Mockito.any());
								
		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}
	
	@Test
	public void testNotificar2VecesANotificadores() {
								
		//le digo al configurador de notificadores que ocurrio un cambio y que debe notificar por los mecanismos
		//de notificacion que se encuentren habilitados
		cn.actualizar(null);
		
		//vuelvo a notificar
		cn.actualizar(null);
								
		//verifico que notifico 1 vez al mecanismo de notificacion por email 
		verify(nEMailSpy, times(2)).actualizar(Mockito.any());
								
		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
		verify(nTelegramSpy, times(2)).actualizar(Mockito.any());
		
	}
	
	@Test
	public void testGetNotificadores() {
		
		//verifico que el get me trae todos los notificadores
		assertTrue(cn.getNotificadores().contains(nEMailSpy));
		assertTrue(cn.getNotificadores().contains(nTelegramSpy));
		
		//verifico que el get no contenga a notificadores no registrados
		assertFalse(cn.getNotificadores().contains(mecanismoDeNotificacionEmail));
		assertFalse(cn.getNotificadores().contains(mecanismoDeNotificacionTelegram));
	}
}

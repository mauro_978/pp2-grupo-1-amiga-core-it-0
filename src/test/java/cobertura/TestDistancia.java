package cobertura;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Ubicacion;
import servicio.CalculadorDeDistancias;

public class TestDistancia {
	
	CalculadorDeDistancias c;
	Ubicacion u1;
	Ubicacion u2;
	
	@Before  
	public void setUp() {   
		c = CalculadorDeDistancias.getInstance();
	}   
	
	@Test
	public void calcularDistancia() {
		u1 = new Ubicacion(55.75, 37.616666);
		u2 = new Ubicacion(55.75, 37.616944);
		assertEquals(17.39, c.distanciaEntre(u1, u2), 0.01);
		assertNotEquals(15.00,c.distanciaEntre(u1, u2), 0.01 );
		assertNotEquals(19.00,c.distanciaEntre(u1, u2), 0.01 );
	}

}

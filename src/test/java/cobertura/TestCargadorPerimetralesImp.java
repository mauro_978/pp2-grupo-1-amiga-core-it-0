package cobertura;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

import configuracion.CargadorPerimetralesImp;
import modelo.Perimetral;
import modelo.Persona;

public class TestCargadorPerimetralesImp {

	CargadorPerimetralesImp cargadorPerimetrales;
	
	@Test
	public void testCargarPerimetralesEscenario() {
		cargadorPerimetrales = new CargadorPerimetralesImp("Escenario.json");
		HashSet<Perimetral> perimetralesCargadas = cargadorPerimetrales.getPerimetrales();
		assertFalse(perimetralesCargadas.isEmpty());
		Persona agresor = new Persona("1", "Jose", "D1");
		Persona agredido = new Persona("2", "Pedro", "D2");
		for(Perimetral p : perimetralesCargadas) {
			assertTrue(p.getId()==0);
			assertTrue(p.getDistanciaPeligro()==5);
			assertTrue(p.getDistanciaPrecaucion()==10);
			assertEquals(p.getAgresor().getDni(), agresor.getDni());
			assertEquals(p.getAgresor().getNombre(), agresor.getNombre());
			assertEquals(p.getAgresor().getIdDispositivo(), agresor.getIdDispositivo());
			
			assertEquals(p.getAgredido().getDni(), agredido.getDni());
			assertEquals(p.getAgredido().getNombre(), agredido.getNombre());
			assertEquals(p.getAgredido().getIdDispositivo(), agredido.getIdDispositivo());
		}
	}
	
	//habria que hacer algunos test mas, uno con un archivo que no tenga ninguna perimetral x ejemplo

}

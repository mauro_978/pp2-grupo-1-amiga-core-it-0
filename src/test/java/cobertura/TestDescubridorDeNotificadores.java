package cobertura;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import configuracion.DescubridorDeNotificadores;

public class TestDescubridorDeNotificadores {
	
	String path;
	String packageName;
	
	@Test
	public void testRutaYPaqueteNoExiste() {
		path = "rutaNoExiste";
		packageName = "noExiste";
		assertTrue(DescubridorDeNotificadores.getNotificadores(path, packageName).isEmpty());
	}
	
	@Test
	public void testRutaNoExiste() {
		path = "rutaNoExiste";
		packageName = "modelo";
		assertTrue(DescubridorDeNotificadores.getNotificadores(path, packageName).isEmpty());
	}
	
	@Test
	public void testPaqueteNoExiste() {
		path = new File("").getAbsolutePath().toString()+"\\bin\\main\\modelo";
		packageName = "noExiste";
		assertTrue(DescubridorDeNotificadores.getNotificadores(path, packageName).isEmpty());
	}

}

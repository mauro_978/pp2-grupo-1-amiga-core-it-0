package cobertura;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import interfacesModelo.ServicioCalculadorDeDistancias;
import modelo.Dispositivo;
import modelo.Perimetral;
import modelo.Persona;
import modelo.Validador;
import setup.CreadorValidadores;

public class TestCreadorValidadores {
	
	CreadorValidadores CV;
	HashMap<Persona, Dispositivo> dispositivos;
	ServicioCalculadorDeDistancias scd;
	
	//no podra crear el validador porque el map de persona, dispositivo no esta inicializado. soltara una NullPointerException
	@Test(expected=NullPointerException.class)
	public void testExcepcionMapPersonaDispositivoNull() {
		CV = new CreadorValidadores(dispositivos, scd);
		Persona agresor = new Persona("40698875","juan","D1");
		Persona agredido = new Persona("42853431","pedro","D2");
		int distanciaPeligro = 10;
		int distanciaPrecaucion = 20;
		Perimetral p = new Perimetral(agresor, agredido, distanciaPeligro, distanciaPrecaucion);
		Object o = CV.crearValidador(p);
	}
	
	//no podra attachear un objecto nulo y soltara una NullPointerException
	@Test(expected=NullPointerException.class)
	public void testExcepcionMapVacio() {
		Persona agresor = new Persona("40698875","juan","D1");
		Persona agredido = new Persona("42853431","pedro","D2");
		int distanciaPeligro = 10;
		int distanciaPrecaucion = 20;
		Perimetral p = new Perimetral(agresor, agredido, distanciaPeligro, distanciaPrecaucion);
		
		dispositivos = new HashMap<Persona, Dispositivo>();
		
		CV = new CreadorValidadores(dispositivos, scd);
		Object o = CV.crearValidador(p);
//		assertTrue(o instanceof Validador);
	}

	@Test
	public void testCreaUnValidador() {
		Persona agresor = new Persona("40698875","juan","D1");
		Persona agredido = new Persona("42853431","pedro","D2");
		int distanciaPeligro = 10;
		int distanciaPrecaucion = 20;
		Perimetral p = new Perimetral(agresor, agredido, distanciaPeligro, distanciaPrecaucion);
		
		Dispositivo d1 = new Dispositivo("D1");
		Dispositivo d2 = new Dispositivo("D2");
		
		dispositivos = new HashMap<Persona, Dispositivo>();
		dispositivos.put(p.getAgredido(), d1);
		dispositivos.put(p.getAgresor(), d2);
	
		CV = new CreadorValidadores(dispositivos, scd);
		Object o = CV.crearValidador(p);
		assertTrue(o instanceof Validador);
		
		Validador v = (Validador) o;
		assertTrue(v.getPerimetral().getAgresor().equals(agresor));
		assertTrue(v.getPerimetral().getAgredido().equals(agredido));
		assertTrue(v.getPerimetral().getDistanciaPeligro()==distanciaPeligro);
		assertTrue(v.getPerimetral().getDistanciaPrecaucion()==distanciaPrecaucion);
	}
}

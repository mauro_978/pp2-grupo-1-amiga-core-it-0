package criteriosDeAceptacion.ImplementacionesNotificadorLleno;

import interfacesModelo.Notificador;
import interfacesModelo.Observable;
import modelo.Validador;

public class ImpNotificadorTelegram implements Notificador{

	String nombre = "telegram";
	
	@Override
	public String getNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		return nombre;
	}

	@Override
	public void actualizar(Observable obs) {
		Validador v = (Validador) obs;
		System.out.println("-------------------------------\n"
				+ "Se envio un mensaje por telegram con los siguientes datos: "+"\nAgredido: " + v.getPerimetral().getAgredido().getNombre() + "\nAgresor: " + v.getPerimetral().getAgresor().getNombre());
	}

}
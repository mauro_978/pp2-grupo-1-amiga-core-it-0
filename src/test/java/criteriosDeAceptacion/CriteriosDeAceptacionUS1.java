package criteriosDeAceptacion;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import interfacesModelo.Observable;
import interfacesModelo.Observer;
import interfacesModelo.ServicioCalculadorDeDistancias;
import modelo.Dispositivo;
import modelo.Perimetral;
import modelo.Persona;
import modelo.Ubicacion;
import modelo.Validador;
import servicio.CalculadorDeDistancias;

@RunWith(MockitoJUnitRunner.class)
public class CriteriosDeAceptacionUS1 {

		Persona per1,per2;
		Perimetral p;
		Dispositivo d1,d2;
		ServicioCalculadorDeDistancias scd;
		Validador v;
		Observer oSpy;
		
		
		public void Escenario() {  
			per1 = new Persona("45019951","juan","D1");
			per2 = new Persona("40219951","juan","D2");
			p = new Perimetral(per1, per2, 5, 10);
			d1 = new Dispositivo(per1.getIdDispositivo());
			d2 = new Dispositivo(per2.getIdDispositivo());
			scd = new CalculadorDeDistancias() {
				@Override 
				public double distanciaEntre(Ubicacion u1, Ubicacion u2){
					double distancia = u1.getLongitud()-u2.getLongitud();
					if(distancia>=0)return distancia;
					return distancia * -1.0;
				}};
			Observer o = new Observer() {

				@Override
				public void actualizar(Observable obs) {
					
				}
			};
			oSpy = Mockito.spy(o);
			
			v = new Validador(d1,d2,p,scd) {
				@Override
				public void notificar(Observable ob) {
					oSpy.actualizar(ob);
				}
			};
			v.attach(o);
		} 
		
		@Test 
		public void ca1() {
			Escenario();
			Ubicacion nuevaUbicacion = new Ubicacion(0.0, 1.0);
			d1.actualizarUbicacion(nuevaUbicacion);
			
			//verifico que nunca se notico al observer
			verify(oSpy, times(0)).actualizar(Mockito.any());
	    }
		
		
		@Test
		public void ca2() {
			Escenario();
			Ubicacion nuevaUbicacion = new Ubicacion(0.0, 1.0);
			Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 8.0);
			
			d1.actualizarUbicacion(nuevaUbicacion);
			d2.actualizarUbicacion(nuevaUbicacion2);
			
			//verifico que nunca se notico al observer
			verify(oSpy, times(0)).actualizar(Mockito.any());
		}
		
		@Test
		public void ca3() {
			Escenario();
			Ubicacion nuevaUbicacion = new Ubicacion(0.0, 1.0);
			Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 4.0);

			d1.actualizarUbicacion(nuevaUbicacion);
			d2.actualizarUbicacion(nuevaUbicacion2);
			
			//verifico que se notico 1 vez al observer
			verify(oSpy, times(1)).actualizar(Mockito.any());
			
		}
		
		@Test
		public void ca4() {
			Escenario();
			Ubicacion nuevaUbicacion = new Ubicacion(0.0, 1.0);
			Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 3.0);
			Ubicacion nuevaUbicacion3 = new Ubicacion(0.0, 4.0);
			
			d1.actualizarUbicacion(nuevaUbicacion);
			d2.actualizarUbicacion(nuevaUbicacion2);
			d2.actualizarUbicacion(nuevaUbicacion3);
			
			//verifico que se notico 2 veces al observer
			verify(oSpy, times(2)).actualizar(Mockito.any());
		}	
	
}

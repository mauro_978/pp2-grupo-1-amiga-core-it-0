package criteriosDeAceptacion;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Test;

import configuracion.DescubridorDeNotificadores;
import interfacesModelo.Notificador;

public class CriteriosDeAceptacionUS2 {

	String ruta;
	String paquete;
	
	public void Escenario1() {
		//busco en una ruta donde no existen implementaciones de notificador
		ruta = new File("").getAbsolutePath().toString()+"//bin//test//criteriosDeAceptacion//ImplementacionesNotificadorVacio";	
		paquete = "criteriosDeAceptacion.ImplementacionesNotificadorVacio";
	}
	
	public void Escenario2() {
		//busco en una ruta donde existen 2 implementaciones de notificador
		ruta = new File("").getAbsolutePath().toString()+"//bin//test//criteriosDeAceptacion//ImplementacionesNotificadorLleno";
		paquete = "criteriosDeAceptacion.ImplementacionesNotificadorLleno";
	}
	
	@Test
	public void ca1() {
		Escenario1();
		List<Notificador> nots = DescubridorDeNotificadores.getNotificadores(ruta, paquete);
		assertTrue(nots.size()==0);
	}
	
	@Test
	public void ca2() {
		Escenario2();
		List<Notificador> nots = DescubridorDeNotificadores.getNotificadores(ruta, paquete);
		assertTrue(nots.size()==2);
	}

}

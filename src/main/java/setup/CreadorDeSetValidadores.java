package setup;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import interfacesModelo.ServicioCalculadorDeDistancias;
import modelo.Dispositivo;
import modelo.Perimetral;
import modelo.Persona;
import modelo.Validador;
import servicio.CalculadorDeDistancias;

public class CreadorDeSetValidadores {
	public CreadorDeSetValidadores() {}
	
	public Set<Validador> getValidadores(Set<Perimetral> perimetrales, Map<Persona, Dispositivo> dispositivos) {
		ServicioCalculadorDeDistancias scd = new CalculadorDeDistancias(); 
		CreadorValidadores cv = new CreadorValidadores(dispositivos, scd);
		HashSet<Validador> validadores = new HashSet<Validador>();
		for(Perimetral p : perimetrales) {
			validadores.add(cv.crearValidador(p));
		}
		return validadores;
	}
}

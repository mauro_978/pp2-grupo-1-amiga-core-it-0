package setup;

import modelo.Dispositivo;
import modelo.Persona;

public class CreadorDispositivos {
	public CreadorDispositivos(){
	}
	
	public Dispositivo crearDispositivo(Persona p) {
		return new Dispositivo(p.getIdDispositivo());
	}
}

package setup;

import java.util.Map;
import java.util.Set;

import configuracion.CargadorPerimetralesImp;
import configuracion.ConfiguradorDeNotificadores;
import modelo.Dispositivo;
import modelo.MonitoreoDispositivos;
import modelo.MonitoreoValidadores;
import modelo.Perimetral;
import modelo.Persona;
import modelo.Validador;

public class Setup {
	private MonitoreoValidadores 		monitoreoValidadores;
	private MonitoreoDispositivos		monitoreoDispositivos;
	private ConfiguradorDeNotificadores configuradorDeNotificadores;
	
	public Setup(String direccionJsonPerimetrales) {
		crearMonitoreos(direccionJsonPerimetrales);
		crearConfiguradorDeNotificadores();
	}
	

	private void crearMonitoreos(String direccionJsonPerimetrales) {
		CargadorPerimetralesImp cp = new CargadorPerimetralesImp(direccionJsonPerimetrales);
		CreadorDeMapDispositivos CMD = new CreadorDeMapDispositivos();
		CreadorDeSetValidadores CSV = new CreadorDeSetValidadores();
		Set<Perimetral> perimetrales = cp.getPerimetrales();
		Map<Persona, Dispositivo> dispositivos = CMD.crearDispositivos(perimetrales);
		Set<Validador> validadores = CSV.getValidadores(perimetrales, dispositivos);
		this.monitoreoDispositivos = new MonitoreoDispositivos(dispositivos);
		this.monitoreoValidadores = new MonitoreoValidadores(validadores);
	}
		
	private void crearConfiguradorDeNotificadores() {
		CreadorConfiguradorDeNotificadores CCdN = new CreadorConfiguradorDeNotificadores();
		this.configuradorDeNotificadores = CCdN.getConfiguradorDeNotificadores();
		this.monitoreoValidadores.attach(this.configuradorDeNotificadores);
	}
	
	public MonitoreoValidadores getMonitoreoDispositivo() {
		return this.monitoreoValidadores;
	}
	
	public MonitoreoDispositivos getMonitoreoValidadores(){
		return this.monitoreoDispositivos;
	}
	
	public ConfiguradorDeNotificadores getConfiguradorDeNotificadores() {
		return this.configuradorDeNotificadores;
	}
}

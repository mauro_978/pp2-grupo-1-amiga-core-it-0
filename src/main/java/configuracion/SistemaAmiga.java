package configuracion;

import java.util.Set;

import interfacesModelo.Notificador;
import interfacesModelo.Observer;
import modelo.Dispositivo;
import modelo.MonitoreoDispositivos;
import modelo.MonitoreoValidadores;
import modelo.Persona;
import modelo.Validador;
import setup.Setup;

public class SistemaAmiga {
	private MonitoreoValidadores 			monitoreoValidadores;
	private MonitoreoDispositivos			monitoreoDispositivos;
	private ConfiguradorDeNotificadores		configuradorDeNotificadores;
	
	public SistemaAmiga(){}
	//holas
	public void inicializar() {
		Setup setup = new Setup("perimetrales.json");
		this.monitoreoValidadores = setup.getMonitoreoDispositivo();
		this.monitoreoDispositivos = setup.getMonitoreoValidadores();
		this.configuradorDeNotificadores = setup.getConfiguradorDeNotificadores();
	}
	
	public void inicializar(String direccionJsonPerimetrales) {
		Setup setup = new Setup(direccionJsonPerimetrales);
		this.monitoreoValidadores = setup.getMonitoreoDispositivo();
		this.monitoreoDispositivos = setup.getMonitoreoValidadores();
		this.configuradorDeNotificadores = setup.getConfiguradorDeNotificadores();
	}
	
	// metodo de la iteracion 1
//	public void habilitarDeshabilitarNotificador(Notificador n, boolean b) {
//		this.configuradorDeNotificadores.habilitarDeshabilitarNotificador(n, b);
//	}
	
	public void attachObserverDispsitivos(Observer o) {
		this.monitoreoDispositivos.attach(o);
	}
	
	public void attachObserverValidadores(Observer o) {
		this.monitoreoValidadores.attach(o);
	}
	public Dispositivo getDispositivo(Persona p) {
		return this.monitoreoDispositivos.getDispositivo(p);
	}
	public Dispositivo getDispositivo(String idDispositivo) {
		return this.monitoreoDispositivos.getDispositivo(idDispositivo);
	}

	public Set<Validador> getValidadores() {
		return this.monitoreoValidadores.getValidadores();
	}
	
	public Set<Notificador> getNotificadores(){
		return configuradorDeNotificadores.getNotificadores();
	}
}

package configuracion;

import java.io.File;
import java.util.ArrayList;

import interfacesModelo.Notificador;

public class DescubridorDeNotificadores {
	
		//recibe un path de donde tiene que detectar los archivos y un nombre de packete de donde 
		//tiene que cargarlos el class forname (ejemplo: path->System.getProperty("user.dir")+"\\bin\\main\\paqueteEspecifico
		//                                               packageName->org.math.x)
		public static ArrayList<Notificador> getNotificadores(String path, String PackageName){
			ArrayList<Notificador> result = new ArrayList<>();
			try {
				for(File f : new File(path).listFiles()){
					if(!f.getName().endsWith(".class")) continue;
					try {
						Class<?> c = Class.forName(PackageName+"."+f.getName().substring(0, f.getName().lastIndexOf(".")));	
						if(!Notificador.isAssignableFrom(c.getClass()))
							throw new RuntimeException();
						result.add((Notificador)c.getDeclaredConstructor().newInstance());
					}catch(Exception ex) {
						ex.printStackTrace();
					}
					
				}
			}catch(NullPointerException ex) {
//				new NullPointerException(ex + "la ruta fue: " + path).printStackTrace();
				ex.printStackTrace();
			}
			return result;
		}
}

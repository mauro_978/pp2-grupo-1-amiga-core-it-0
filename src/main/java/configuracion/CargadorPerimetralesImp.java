package configuracion;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.HashSet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import interfacesModelo.InterfaceCargadorPerimetrales;
import modelo.Perimetral;

public class CargadorPerimetralesImp implements InterfaceCargadorPerimetrales {
	HashSet<Perimetral> 		perimetrales;
	
	public CargadorPerimetralesImp(String nombreJSON) {
		perimetrales = new HashSet<Perimetral>();
		cargarPerimetrales(nombreJSON);
	}
	@Override
	public HashSet<Perimetral> getPerimetrales() {
		return this.perimetrales;
	}
	
	private void cargarPerimetrales(String nombreJSON) {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		FileReader reader;
		try {
			reader = new FileReader(nombreJSON);
			Type t = new TypeToken<HashSet<Perimetral>>() {}.getType();
			HashSet<Perimetral> r = gson.fromJson(reader, t);
			this.perimetrales = r;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}

package configuracion;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import interfacesModelo.Notificador;
import interfacesModelo.Observable;
import interfacesModelo.Observer;

public class ConfiguradorDeNotificadores implements Observer{
	private Set<Notificador>		notificadores;
	
	public ConfiguradorDeNotificadores(List<Notificador> notificadores) {
		this.notificadores = new HashSet<Notificador>();
		for(Notificador n :notificadores) {
			this.notificadores.add(n);
		}
	}
	
	public Set<Notificador> getNotificadores(){
		return this.notificadores;
	}
	//pertenece a la itereacion 1. Se saca para claridad de la documentacion
//	public void habilitarDeshabilitarNotificador (Notificador not, boolean estado) {
//		this.notificadores.put(not, estado);
//	}

	@Override
	public void actualizar(Observable obs) {
		for(Notificador n : this.notificadores){
				n.actualizar(obs);
		}
	}
}

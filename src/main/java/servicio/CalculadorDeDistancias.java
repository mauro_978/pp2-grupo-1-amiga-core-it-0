package servicio;

import interfacesModelo.ServicioCalculadorDeDistancias;
import modelo.Ubicacion;

public class CalculadorDeDistancias implements ServicioCalculadorDeDistancias{
	private static CalculadorDeDistancias instance;
	
	public CalculadorDeDistancias() {
		
	}
	public double distanciaEntre(Ubicacion denunciante, Ubicacion denunciado) {  
    	//double radioTierra = 3958.75;//en millas  
//      double radioTierra = 6371;//en kil�metros  
        double radioTierra = 6371000;//en metros 
        double distanciaLat = Math.toRadians(denunciado.getLatitud() - denunciante.getLatitud());  
        double distanciaLng = Math.toRadians(denunciado.getLongitud() - denunciante.getLongitud());  
        double sindLat = Math.sin(distanciaLat / 2);  
        double sindLng = Math.sin(distanciaLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                    * Math.cos(Math.toRadians(denunciante.getLatitud())) * Math.cos(Math.toRadians(denunciado.getLatitud()));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
       
        return distancia;  
    }  
	
	public static CalculadorDeDistancias getInstance() {
		if(instance == null) {
			instance = new CalculadorDeDistancias();
		}
		return instance;
	}
	
}

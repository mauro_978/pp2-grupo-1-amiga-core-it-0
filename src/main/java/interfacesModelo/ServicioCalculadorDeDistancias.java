package interfacesModelo;

import modelo.Ubicacion;

public interface ServicioCalculadorDeDistancias {

	public double distanciaEntre(Ubicacion denunciante, Ubicacion denunciado);
}

package interfacesModelo;

import java.util.HashSet;

import modelo.Perimetral;

public interface InterfaceCargadorPerimetrales {
	public HashSet<Perimetral> getPerimetrales();
}

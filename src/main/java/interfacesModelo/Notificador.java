package interfacesModelo;

public interface Notificador extends Observer {
	
	public static boolean isAssignableFrom(Class<?> class1) {
		return class1.isInstance(Notificador.class);
	}
	public String getNombre();
}

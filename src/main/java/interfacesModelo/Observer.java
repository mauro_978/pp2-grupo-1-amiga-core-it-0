package interfacesModelo;

public interface Observer {
	public void actualizar(Observable obs);
}

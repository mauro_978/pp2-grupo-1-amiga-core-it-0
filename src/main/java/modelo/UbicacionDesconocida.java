package modelo;

public class UbicacionDesconocida extends Ubicacion{

	public UbicacionDesconocida() {
		super(0.0, 0.0);
	}
	
	@Override
	public boolean isDesconocida() {
		return true;
	}
}

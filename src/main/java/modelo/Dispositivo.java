package modelo;

import java.util.HashSet;
import java.util.Set;

import interfacesModelo.Observable;
import interfacesModelo.Observer;
import interfacesModelo.Proxy;
import modelo.Dispositivo;

public class Dispositivo implements Proxy, Observable {
	private Set<Observer> 		observadores;
	private String 				idDispositivo;
	private Ubicacion			ubicacionDispositivo;
	
	public Dispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
		this.ubicacionDispositivo = new UbicacionDesconocida();
		this.observadores = new HashSet<Observer>();
	}
	
	@Override
	public void attach(Observer o) {
		this.observadores.add(o);
	}
	
	@Override
	public void dettach(Observer o) {
		this.observadores.remove(o);
	}

	@Override
	public void notificar(Observable obs) {
		for(Observer o :this.observadores) {
			o.actualizar(obs);
		}
	}

	public void actualizarUbicacion(Ubicacion u) {
		this.ubicacionDispositivo = u;
		notificar(this);
	}

	public String getIdDispositivo() {
		return idDispositivo;
	}

	public Ubicacion getUbicacionDispositivo() {
		return ubicacionDispositivo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDispositivo == null) ? 0 : idDispositivo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dispositivo other = (Dispositivo) obj;
		if (idDispositivo == null) {
			if (other.getIdDispositivo() != null)
				return false;
		} else if (!idDispositivo.equals(other.getIdDispositivo()))
			return false;
		return true;
	}

	
}

package modelo;

import java.util.HashSet;
import java.util.Set;

import interfacesModelo.Observable;
import interfacesModelo.Observer;

public class MonitoreoValidadores implements Observable, Observer{
	private Set<Validador>				validadores;
	private Set<Observer>				observadores;

	public MonitoreoValidadores(Set<Validador> validadores) {
		for(Validador v : validadores) {
			v.attach(this);
		}
		this.validadores = validadores;
		this.observadores = new HashSet<Observer>();
	}

	public Set<Validador> getValidadores() {
		return validadores;
	}
	
	public Set<Observer> getObservers(){
		return this.observadores;
	}

	@Override
	public void attach(Observer o) {
		this.observadores.add(o);
	}

	@Override
	public void dettach(Observer o) {
		this.observadores.remove(o);
	}

	@Override
	public void notificar(Observable observable) {
		for(Observer o: this.observadores) {
			o.actualizar(observable);
		}
	}

	@Override
	public void actualizar(Observable obs) {
		this.notificar(obs);		
	}
}

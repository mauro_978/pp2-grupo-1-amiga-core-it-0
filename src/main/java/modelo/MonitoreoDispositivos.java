package modelo;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import interfacesModelo.Observable;
import interfacesModelo.Observer;

public class MonitoreoDispositivos implements Observable, Observer {
		private Map<Persona,Dispositivo>	dispositivos;
		private Set<Observer>				observadores;
		
		public MonitoreoDispositivos(Map<Persona, Dispositivo> dispositivos) {
			this.dispositivos = dispositivos;
			for(Dispositivo d : this.dispositivos.values()) {
				d.attach(this);
			}
			this.observadores = new HashSet<Observer>();
		}
		
		public Dispositivo getDispositivo(Persona p) {
			return this.dispositivos.get(p);
		}
		
		public Dispositivo getDispositivo(String idDispositivo) {
			for(Dispositivo d : this.dispositivos.values()) {
				if(d.getIdDispositivo().equals(idDispositivo))return d;
			}
			return null;
		}
		
		@Override
		public void attach(Observer o) {
			this.observadores.add(o);
		}

		@Override
		public void dettach(Observer o) {
			this.observadores.remove(o);
		}

		@Override
		public void notificar(Observable obs) {
			for(Observer o : this.observadores) {
				o.actualizar(obs);
			}
		}

		@Override
		public void actualizar(Observable obs) {
			this.notificar(obs);
		}
}

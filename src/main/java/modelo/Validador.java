package modelo;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import interfacesModelo.Observable;
import interfacesModelo.Observer;
import interfacesModelo.ServicioCalculadorDeDistancias;

public class Validador implements Observer, Observable {

	private Observer		observador;
	private Perimetral 		p;
	private int distanciaPeligro;
	Set<Dispositivo> participantesDePerimetral = new HashSet<Dispositivo>();
	ServicioCalculadorDeDistancias scd;

	
	public Validador(Dispositivo agresor, Dispositivo agredido, Perimetral p, ServicioCalculadorDeDistancias scd) {
		agresor.attach(this);
		agredido.attach(this);
		this.p=p;
		this.participantesDePerimetral.add(agresor);
		this.participantesDePerimetral.add(agredido);
		distanciaPeligro = p.getDistanciaPeligro();
		this.scd = scd;
	}
	
	
	@Override
	public void actualizar(Observable obs) {
		validar();
	}
	
	private void validar() {
		assert this.participantesDePerimetral.size() == 2: "La cantidad de dispositivos de la perimetral es invalida";
		Ubicacion u1, u2;
		Iterator<Dispositivo> value = participantesDePerimetral.iterator();
		u1 = value.next().getUbicacionDispositivo();
		u2 = value.next().getUbicacionDispositivo();
		if(!u1.isDesconocida()&&!u2.isDesconocida()) {
			double distancia = scd.distanciaEntre(u1, u2);
			if(distancia <= distanciaPeligro) {
				notificar(this);
			}
		}
	}
	
	public Perimetral getPerimetral() {
		return this.p;
	}


	@Override
	public void attach(Observer o) {
		this.observador = o;
	}
	@Override
	public void dettach(Observer o) {
		//tendr� un unico observer
	}

	@Override
	public void notificar(Observable o) {
		this.observador.actualizar(o);
	}

}

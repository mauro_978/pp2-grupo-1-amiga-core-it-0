package modelo;

public class Perimetral {
	private int id;
	private int distanciaPeligro;
	private int distanciaPrecaucion;
	private Persona agresor;
	private Persona agredido;
	
	public Perimetral(Persona agresor, Persona agredido, int distanciaPeligro, int distanciaPrecaucion) {
		this.id = 0;
		this.agresor = agresor;
		this.agredido = agredido;
		this.distanciaPeligro = distanciaPeligro;
		this.distanciaPrecaucion = distanciaPrecaucion;
	}

	public int getId() {
		return id;
	}

	public Persona getAgresor() {
		return agresor;
	}

	public Persona getAgredido() {
		return agredido;
	}
	
	public int getDistanciaPeligro() {
		return distanciaPeligro;
	}
	
	public int getDistanciaPrecaucion() {
		return this.distanciaPrecaucion;
	}
	
}
